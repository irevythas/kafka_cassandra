from cassandra import ConsistencyLevel
from cassandra.cluster import Cluster
from cassandra.query import SimpleStatement
KEYSPACE = "testkeyspace"

cluster = Cluster(['127.0.0.1'])
session = cluster.connect()

session.execute("""
    CREATE KEYSPACE IF NOT EXISTS %s
    WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '2' }
    """ % KEYSPACE)


session.set_keyspace(KEYSPACE)


session.execute("""
    CREATE TABLE IF NOT EXISTS numtest (
        thekey text,
        col1 text,
        PRIMARY KEY (thekey, col1)
    )
    """)

query = SimpleStatement("""
    INSERT INTO numtest (thekey, col1)
    VALUES (%(key)s, %(a)s)
    """, consistency_level=ConsistencyLevel.ONE)

prepared = session.prepare("""
    INSERT INTO numtest (thekey, col1)
    VALUES (?, ?)
    """)

for i in range(10):
    session.execute(query, dict(key="key%d" % i, a='a'))
    session.execute(prepared, ("key%d" % i, 'b'))

future = session.execute_async("SELECT * FROM numtest")



rows = future.result()

for row in rows:
    print('\t'.join(row))


session.execute("DROP KEYSPACE " + KEYSPACE)    

