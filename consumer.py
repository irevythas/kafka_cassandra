# Edit vi /etc/cassandra/cassandra-env.sh -> JVM_OPTS="$JVM_OPTS -Djava.rmi.server.hostname=127.0.0.1"
from kafka import KafkaConsumer
from cassandra import ConsistencyLevel
from cassandra.cluster import Cluster
from cassandra.query import SimpleStatement
from json import loads

KEYSPACE = "testkeyspace"

cluster = Cluster(['127.0.0.1'])
session = cluster.connect()

session.execute("""
    CREATE KEYSPACE IF NOT EXISTS %s
    WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '2' }
    """ % KEYSPACE)

session.set_keyspace(KEYSPACE)

session.execute("""
    CREATE TABLE IF NOT EXISTS numtest (
        thekey text,
        col1 text,
        PRIMARY KEY (thekey, col1)
    )
    """)

query = SimpleStatement("""
    INSERT INTO numtest (thekey, col1)
    VALUES (%(key)s, %(a)s)
    """, consistency_level=ConsistencyLevel.ONE)

prepared = session.prepare("""
    INSERT INTO numtest (thekey, col1)
    VALUES (?, ?)
    """)

consumer = KafkaConsumer(
    'numtest',
     bootstrap_servers=['localhost:9092'],
     auto_offset_reset='earliest',
     enable_auto_commit=True,
     group_id='my-group',
     value_deserializer=lambda x: loads(x.decode('utf-8')))

i=0
for message in consumer:
    message = message.value
    session.execute(query, dict(key="key_%d" % i, a=message['number']))
    session.execute(prepared, ("key_%d" % i, message['number']))
#    print('{} added '.format(message))
    print(message['number'])
    i = i + 1
